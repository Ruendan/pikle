function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

let data = null;
let chosenBulborb = null;

function pick(bulborbName) {
    const testBulborb = data.find(bulborb => bulborb.name == bulborbName);
    if(!testBulborb) return;
    
    const row = document.createElement("div");
    row.classList.add("row")
    row.classList.add("center")

    const empty1 = document.createElement("div")
    empty1.classList.add("col")
    empty1.classList.add("s1")
    row.appendChild(empty1)

    const icon = document.createElement("div")
    const img = document.createElement("img")
    img.src = testBulborb.icon
    icon.classList.add("col")
    icon.classList.add("s1")
    icon.appendChild(img)
    row.appendChild(icon)

    const name = document.createElement("div")
    name.innerText = testBulborb.name
    testBulborb.name == chosenBulborb.name ? name.classList.add("green") : name.classList.add("red")
    name.classList.add("col")
    name.classList.add("s1")
    row.appendChild(name)

    const scientific_name = document.createElement("div")
    scientific_name.innerText = testBulborb.scientific_name
    testBulborb.scientific_name == chosenBulborb.scientific_name ? scientific_name.classList.add("green") : scientific_name.classList.add("red")
    scientific_name.classList.add("col")
    scientific_name.classList.add("s1")
    row.appendChild(scientific_name)

    const family = document.createElement("div")
    family.innerText = testBulborb.family
    testBulborb.family == chosenBulborb.family ? family.classList.add("green") : family.classList.add("red")
    family.classList.add("col")
    family.classList.add("s1")
    row.appendChild(family)

    const active_during = document.createElement("div")
    active_during.innerText = testBulborb.active_during
    testBulborb.active_during == chosenBulborb.active_during ? active_during.classList.add("green") : active_during.classList.add("red")
    active_during.classList.add("col")
    active_during.classList.add("s1")
    row.appendChild(active_during)

    const weight = document.createElement("div")
    weight.innerText = testBulborb.weight
    testBulborb.weight == chosenBulborb.weight ? weight.classList.add("green") : weight.classList.add("red")
    weight.classList.add("col")
    weight.classList.add("s1")
    row.appendChild(weight)

    const max_carriers = document.createElement("div")
    max_carriers.innerText = testBulborb.max_carriers
    testBulborb.max_carriers == chosenBulborb.max_carriers ? max_carriers.classList.add("green") : max_carriers.classList.add("red")
    max_carriers.classList.add("col")
    max_carriers.classList.add("s1")
    row.appendChild(max_carriers)

    const sparklium_value = document.createElement("div")
    sparklium_value.innerText = testBulborb.sparklium_value
    testBulborb.sparklium_value == chosenBulborb.sparklium_value ? sparklium_value.classList.add("green") : sparklium_value.classList.add("red")
    sparklium_value.classList.add("col")
    sparklium_value.classList.add("s1")
    row.appendChild(sparklium_value)

    const seeds = document.createElement("div")
    seeds.innerText = testBulborb.seeds
    testBulborb.seeds == chosenBulborb.seeds ? seeds.classList.add("green") : seeds.classList.add("red")
    seeds.classList.add("col")
    seeds.classList.add("s1")
    row.appendChild(seeds)

    const empty2 = document.createElement("div")
    empty2.classList.add("col")
    empty2.classList.add("s2")
    row.appendChild(empty2)

    document.querySelector("#bulborb-display").appendChild(row);
}

function init(bulborbs) {
    data = bulborbs;
    chosenBulborb = bulborbs[getRandomIntInclusive(0, bulborbs.length)]

    const options = {};
    bulborbs.forEach(item => options[item.name] = item.icon);
    const elems = document.querySelectorAll('#bulborb-input');
    const instances = M.Autocomplete.init(elems, {"data":options});

    document.querySelector("#bulborb-form").addEventListener('submit', event => {
        event.preventDefault();
        pick(document.querySelector("#bulborb-input").value)
    })
}

fetch('./bulborbs.json')
    .then((response) => response.json())
    .then((json) => init(json));